<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:default>
    <jsp:body>
        <h1>Alunos do sistema</h1>
        <table style="width:100%">
            <tr>
                <th>Nome</th>
                <th>Idade</th> 
                <th>Matricula</th>
                <th>Media Geral</th>
                <th>Operacoes</th>
            </tr>
            <c:forEach items="${alunos}" var="aluno">
                <tr>
                    <td align="center">${aluno.nome}</td>
                    <td align="center">${aluno.idade}</td> 
                    <td align="center">${aluno.matricula}</td>
                    <td align="center">${aluno.media}</td>
                    <td align="center"> 
                        <a href="/AppNotas/disciplinasNotas?matricula=${aluno.matricula}">Disciplinas Mat.</a> 
                        | 
                        <a href="/AppNotas/removerAluno?matricula=${aluno.matricula}">Remover</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <br/>
        <a href="${pageContext.request.contextPath}">Voltar</a>
    </jsp:body>
</t:default>
