<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:default>
    <jsp:body>
        <h1>Disciplina: ${disciplina}</h1>
        <form method="POST" action="matricular">
            Matricula do aluno:
            <input type="hidden" name="disciplina" id="disciplina" value="${disciplina}">
            <input type="text" name="matriculaAluno" />
            <input type="submit" value="Matricular na disciplina" />
        </form>
        <br/><a href="${pageContext.request.contextPath}">Inicio</a>
    </jsp:body>
</t:default>
