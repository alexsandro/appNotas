 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@page contentType="text/html" pageEncoding="UTF-8"%>
 
 <c:if test="${usuarioLogado != null}">
    <c:redirect url="/index.jsp"/>
</c:if>
  <!DOCTYPE html>
  <html>
      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <title>Login</title>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
      </head>
      <body>
          <h1>Login</h1>
          <c:if test="${contemErro}">
              <div id="erro">
                  <ul>
                          <li> ${mensagem} </li>
                  </ul>
              </div>
          </c:if>
          <form method="post" action="login">
              <table>
                  <tr>
                      <th>Login: </th>
                      <td><input type="text" name="login" value="${login}"/></td>
                  </tr>
                  <tr>
                      <th>Senha: </th>
                      <td><input type="password" name="senha"/></td>
                  </tr>
                  <tr>
                      <td colspan="2"> 
                          <input type="submit" name="bt_autenticar" value="Autenticar"/>
                      </td>
                  </tr>
              </table>
          </form>
      </body>
  </html>