<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:default>
    <jsp:body>
        <h1>Notas</h1>
        
        <form method="POST" action="addNota">
            <input type="hidden" name="matricula" id="matricula" value="${matricula}">
           <input type="hidden" name="disciplina" value="${disciplina}">
            
            Nota 1:
            <input type="text" name="nota1" value="${nota1}"/><br/>
            Nota 2: 
            <input type="text" name="nota2" value="${nota2}"/><br/>
            Nota 3:
            <input type="text" name="nota3" value="${nota3}"/><br/>
            
            <input type="submit" value="Salvar" />
        </form>
    </jsp:body>
</t:default>
