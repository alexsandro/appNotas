<%@tag description="default" pageEncoding="UTF-8"%>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${usuarioLogado == null}">
    <c:redirect url="/login.jsp"/>
</c:if>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="${pageContext.request.contextPath}">App Notas</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="${pageContext.request.contextPath}">Inicio</a></li>
                        <li><a href="${pageContext.request.contextPath}/disciplinaForm.jsp">Cadastrar Disciplina</a></li>
                        <li><a href="${pageContext.request.contextPath}/alunoForm.jsp">Cadastrar Aluno</a></li>
                        <li><a href="${pageContext.request.contextPath}/disciplinas">Listar Disciplinas</a></li>
                        <li><a href="${pageContext.request.contextPath}/alunos">Listar Alunos</a></li>

                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="#">Olá - ${usuarioLogado.login}</a></li>
                        <li><a href="${pageContext.request.contextPath}/logout">Sair</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <div class="container-fluid">
            <jsp:doBody/>
        </div>
        <jsp:invoke fragment="footer"/>
    </body>
</html>