<%@page contentType="text/html" pageEncoding="UTF-8" isErrorPage="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="/public/css/error.css" />" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    
    <body>
        <div class="container bootstrap snippet">
            <div class="row">
                <div class="col-md-12">
                    <div style="margin-top:10px;">
                        <div class="col-md-7 col-md-offset-1 pull-right">
                            <img class="img-error" src="https://bootdey.com/img/Content/fdfadfadsfadoh.png">
                            <h2>500 Erro Interno</h2>
                            <p>Desculpe, ocorreu um erro interno!</p>
                            <div class="error-actions">
                                <a href="${pageContext.request.contextPath}" class="btn btn-primary btn-lg">
                                    <span class="glyphicon glyphicon-arrow-left"></span>
                                    Inicio
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
