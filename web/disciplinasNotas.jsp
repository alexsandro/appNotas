<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:default>
    <jsp:body>
        <h1>Disciplinas matriculadas para o aluno de matricula: ${matricula}</h1>
        <table style="width:100%">
            <tr>
                <th>Nome</th>
                <th>Acao</th>
            </tr>
            <c:forEach items="${disciplinas}" var="disciplina">
                <tr>
                    <td align="center">${disciplina.nome}</td>
                    <td align="center"> 
                        <a href="${pageContext.request.contextPath}/addNotas?matricula=${matricula}&disciplina=${disciplina.nome}">Notas</a> </td>
                </tr>
            </c:forEach>
        </table>
        <br/>
        <a href="${pageContext.request.contextPath}">Voltar</a>
    </jsp:body>
</t:default>
