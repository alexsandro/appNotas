/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.estacio.notas.repositorio;

import br.com.estacio.notas.model.Aluno;
import br.com.estacio.notas.model.Disciplina;
import java.util.ArrayList;
import java.util.List;

public class Alunos {
    private static List<Aluno> db = new ArrayList<>();
    
    public static void addAluno(Aluno aluno) {
        Alunos.db.add(aluno);
    }
     
    public static List<Aluno> getAlunos() {
        return Alunos.db;
    }
    
    public static Aluno getAlunoPorMatricula(String matricula) {
        for (Aluno a : Alunos.db) {
            if(a.getMatricula().equals(matricula)) return a;
        }
        return null;
    }

    public static void update(Aluno atual) {
        Aluno aux = null;
        for (Aluno a : Alunos.db) {
            if(a.getMatricula().equals(atual.getMatricula())) {
                aux = a;
                break;
            }
        }
        
        if (aux != null) {
            Alunos.db.remove(aux);
            Alunos.db.add(aux);
        }
    }

    public static void removerAlunoPorMatricula(String matricula) {
        Aluno aux = null;
        for (Aluno a : Alunos.db) {
            if(a.getMatricula().equals(matricula)) {
                aux = a;
                break;
            }
        }
        
        if (aux != null) {
            Alunos.db.remove(aux);
        }
    }
    
    public static Disciplina getDisciplinaMatriculada(Aluno aluno, String nome) {
        for (Disciplina d : aluno.getDisciplinasMatriculadas()) {
            if(d.getNome().equals(nome)) return d;
        }
        return null;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }
    
    public static void updateDisciplinaAluno(Aluno aluno, Disciplina disciplina) {
        List<Disciplina> old = aluno.getDisciplinasMatriculadas();
        
        Disciplina disc = getDisciplinaMatriculada(aluno, disciplina.getNome());
        
        old.remove(disc);
        old.add(disciplina);
        
        aluno.setDisciplinasMatriculadas(old);
        update(aluno);
    }
    
}
