/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.estacio.notas.repositorio;

import br.com.estacio.notas.model.Disciplina;
import java.util.ArrayList;
import java.util.List;

public class Disciplinas {
    
    private static List<Disciplina> db = new ArrayList<>();
    
    public static void addDisciplina(Disciplina disciplina) {
        db.add(disciplina);
    }
    
    public static Disciplina getDisciplinaPorNome(String nome) {
         Disciplina disciplinaRetorno = null;
         if (nome != null) {
            for (Disciplina disciplina : db) {
                if (nome.equals(disciplina.getNome())) {
                    disciplinaRetorno = disciplina;
                    break;
                }
            }
        }
        return disciplinaRetorno;
    }
    
    public static void removerDisciplina(String nome) {
        Disciplina disciplinaRemover = getDisciplinaPorNome(nome);
        if (disciplinaRemover != null) {
           db.remove(disciplinaRemover);
        }
    }
    
    public static List<Disciplina> listarTodasAsDisciplinas() {
        return db;
    }
    
}
