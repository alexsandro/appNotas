/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.estacio.notas.dao;

import java.util.List;

public interface DAORepository<T> {
    
    public void cadastrar(T objeto);

    public T getObjeto(String identificador);

    public List<T> listar();

    public void deletar(String identificador);

    public void atualizar(T objeto);
}
