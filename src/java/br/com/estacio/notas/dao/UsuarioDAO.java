package br.com.estacio.notas.dao;

import br.com.estacio.notas.conexao.ConectionFactory;
import br.com.estacio.notas.model.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDAO implements DAORepository<Usuario> {

    public Usuario getObjeto(String login) {
        Connection conn = ConectionFactory.conect();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("SELECT NOME, LOGIN, SENHA, EHPROFESSOR FROM USUARIO WHERE LOGIN = ?");
            ps.setString(1, login);
            rs = ps.executeQuery();
            if (rs.next()) {
                return this.criarObjeto(rs);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        } finally {
            ConectionFactory.close(rs, ps, conn);
        }
        return null;
    }

    @Override
    public List<Usuario> listar() {
        List<Usuario> lista = null;
        Connection conn = ConectionFactory.conect();
        Statement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.createStatement();
            rs = ps.executeQuery("SELECT NOME, LOGIN, SENHA, PROFESSOR FROM USUARIO");
            lista = new ArrayList<>();
            while (rs.next()) {
                lista.add(this.criarObjeto(rs));
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        } finally {
            ConectionFactory.close(rs, ps, conn);
        }
        return lista;
    }

    private Usuario criarObjeto(ResultSet rs) throws SQLException {
        Usuario usuario = new Usuario();
        usuario.setNome(rs.getString("NOME"));
        usuario.setLogin(rs.getString("LOGIN"));
        usuario.setSenha(rs.getString("SENHA"));
        usuario.setEhProfessor(rs.getString("EHPROFESSOR"));

        return usuario;
    }

    @Override
    public void deletar(String login) {
        Connection conn = ConectionFactory.conect();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("DELETE FROM USUARIO WHERE LOGIN = ?");
            ps.setString(1, login);
            ps.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } finally {
            ConectionFactory.close(null, ps, conn);
        }
    }

    @Override
    public void atualizar(Usuario usuario) {
        Connection conn = ConectionFactory.conect();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("UPDATE FROM USUARIO SET NOME = ?, SENHA = ?, EHPROFESSOR = ? WHERE LOGIN = ?");
            ps.setString(1, usuario.getNome());
            ps.setString(2, usuario.getSenha());
            ps.setString(3, usuario.getEhProfessor());
            ps.setString(4, usuario.getLogin());
            ps.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } finally {
            ConectionFactory.close(null, ps, conn);
        }
    }
    
    @Override
    public void cadastrar(Usuario usuario) {
        Connection conn = ConectionFactory.conect();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("INSERT INTO USUARIO (NOME, SENHA, EHPROFESSOR, LOGIN) VALUES (?, ?, ?, ?)");
            ps.setString(1, usuario.getNome());
            ps.setString(2, usuario.getSenha());
            ps.setString(3, usuario.getEhProfessor());
            ps.setString(4, usuario.getLogin());
            ps.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } finally {
            ConectionFactory.close(null, ps, conn);
        }
    }

}
