/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.estacio.notas.dao;

import br.com.estacio.notas.conexao.ConectionFactory;
import br.com.estacio.notas.model.Aluno;
import br.com.estacio.notas.model.AlunoDisciplina;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AlunoDisciplinaDAO implements DAORepository<AlunoDisciplina> {

    public AlunoDisciplina getObjeto(String matricula) {
//        Connection conn = ConectionFactory.conect();
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        try {
//            ps = conn.prepareStatement("SELECT NOME, IDADE, MATRICULA, MEDIA FROM ALUNO WHERE MATRICULA = ?");
//            ps.setString(1, matricula);
//            rs = ps.executeQuery();
//            if (rs.next()) {
//                Aluno aluno = new Aluno();
//                aluno.setNome(rs.getString("NOME"));
//                aluno.setIdade(rs.getInt("IDADE"));
//                aluno.setMatricula(rs.getString("MATRICULA"));
//                aluno.setMedia(rs.getFloat("MEDIA"));
//
//                return aluno;
//            }
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//        } finally {
//            ConectionFactory.close(rs, ps, conn);
//        }
        return null;
    }

    @Override
    public List<AlunoDisciplina> listar() {
//        List<Aluno> lista = null;
//        Connection conn = ConectionFactory.conect();
//        Statement ps = null;
//        ResultSet rs = null;
//        try {
//            ps = conn.createStatement();
//            rs = ps.executeQuery("SELECT NOME, IDADE, MATRICULA, MEDIA FROM ALUNO");
//            lista = new ArrayList<>();
//            while (rs.next()) {
//                Aluno aluno = new Aluno();
//                aluno.setNome(rs.getString("NOME"));
//                aluno.setIdade(rs.getInt("IDADE"));
//                aluno.setMatricula(rs.getString("MATRICULA"));
//                aluno.setMedia(rs.getFloat("MEDIA"));
//
//                lista.add(aluno);
//            }
//        } catch (SQLException exception) {
//            exception.printStackTrace();
//        } finally {
//            ConectionFactory.close(rs, ps, conn);
//        }
        return null;
    }
    

    public List<AlunoDisciplina> listarPorMatricula(String matricula) {
        List<AlunoDisciplina> lista = null;
        Connection conn = ConectionFactory.conect();
        Statement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.createStatement();
            rs = ps.executeQuery("SELECT DISCIPLINA FROM ALUNO_DISCIPLINA WHERE MATRICULA = " + matricula);
            lista = new ArrayList<>();
            while (rs.next()) {
                AlunoDisciplina ad = new AlunoDisciplina();
                ad.setMatricula(matricula);
               ad.setNomeDisciplina(rs.getString("DISCIPLINA"));

                lista.add(ad);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        } finally {
            ConectionFactory.close(rs, ps, conn);
        }
        return lista;
    }

    @Override
    public void deletar(String matricula) {
        Connection conn = ConectionFactory.conect();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("DELETE FROM ALUNO_DISCIPLINA WHERE MATRICULA = ? AND DISCIPLINA = ?");
            ps.setString(1, matricula);
           // ps.setString(2, disciplina);
            ps.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } finally {
            ConectionFactory.close(null, ps, conn);
        }
    }
    
    public void deletar(String matricula,  String disciplina) {
        Connection conn = ConectionFactory.conect();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("DELETE FROM ALUNO_DISCIPLINA WHERE MATRICULA = ? AND DISCIPLINA = ?");
            ps.setString(1, matricula);
            ps.setString(2, disciplina);
            ps.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } finally {
            ConectionFactory.close(null, ps, conn);
        }
    }

    public void atualizar(Aluno aluno) {
        Connection conn = ConectionFactory.conect();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("UPDATE FROM ALUNO SET IDADE = ?, NOME = ?, MEDIA = ? WHERE MATRICULA = ?");
            ps.setInt(1, aluno.getIdade());
            ps.setString(2, aluno.getNome());
            ps.setFloat(3, aluno.getMedia());
            ps.setString(4, aluno.getMatricula());

            ps.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } finally {
            ConectionFactory.close(null, ps, conn);
        }
    }
    
    @Override
    public void cadastrar(AlunoDisciplina ad) {
        Connection conn = ConectionFactory.conect();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("INSERT INTO ALUNO_DISCIPLINA (MATRICULA, DISCIPLINA) VALUES (?, ?)");
            ps.setString(1, ad.getMatricula());
            ps.setString(2, ad.getNomeDisciplina());

            ps.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } finally {
            ConectionFactory.close(null, ps, conn);
        }
    }

    @Override
    public void atualizar(AlunoDisciplina objeto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
