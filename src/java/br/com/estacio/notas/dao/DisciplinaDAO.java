/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.estacio.notas.dao;

import br.com.estacio.notas.conexao.ConectionFactory;
import br.com.estacio.notas.model.Disciplina;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DisciplinaDAO implements DAORepository<Disciplina> {

    public Disciplina getObjeto(String nome) {
        Connection conn = ConectionFactory.conect();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("SELECT NOME, NOTA1, NOTA2, NOTA3 FROM DISCIPLINA WHERE NOME = ?");
            ps.setString(1, nome);
            rs = ps.executeQuery();
            if (rs.next()) {
                Disciplina disciplina = new Disciplina();
                disciplina.setNome(rs.getString("NOME"));
                disciplina.setNota1(rs.getString("NOTA1"));
                disciplina.setNota2(rs.getString("NOTA2"));
                disciplina.setNota3(rs.getString("NOTA3"));

                return disciplina;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            ConectionFactory.close(rs, ps, conn);
        }
        return null;
    }

    @Override
    public List<Disciplina> listar() {
        List<Disciplina> lista = null;
        Connection conn = ConectionFactory.conect();
        Statement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.createStatement();
            rs = ps.executeQuery("SELECT NOME, NOTA1, NOTA2, NOTA3 FROM DISCIPLINA");
            lista = new ArrayList<>();
            while (rs.next()) {
                Disciplina disciplina = new Disciplina();
                disciplina.setNome(rs.getString("NOME"));
                disciplina.setNota1(rs.getString("NOTA1"));
                disciplina.setNota2(rs.getString("NOTA2"));
                disciplina.setNota3(rs.getString("NOTA3"));

                lista.add(disciplina);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        } finally {
            ConectionFactory.close(rs, ps, conn);
        }
        return lista;
    }

    @Override
    public void deletar(String nome) {
        Connection conn = ConectionFactory.conect();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("DELETE FROM DISCIPLINA WHERE NOME = ?");
            ps.setString(1, nome);
            ps.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } finally {
            ConectionFactory.close(null, ps, conn);
        }
    }

    @Override
    public void atualizar(Disciplina disciplina) {
        Connection conn = ConectionFactory.conect();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("UPDATE FROM DISCIPLINA SET NOTA1 = ?, NOTA2 = ?, NOTA3 = ? WHERE NOME = ?");
            ps.setString(1, disciplina.getNota1());
            ps.setString(2, disciplina.getNota2());
            ps.setString(3, disciplina.getNota3());
            ps.setString(4, disciplina.getNome());

            ps.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } finally {
            ConectionFactory.close(null, ps, conn);
        }
    }
    
      @Override
    public void cadastrar(Disciplina disciplina) {
        Connection conn = ConectionFactory.conect();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("INSERT INTO DISCIPLINA (NOTA1, NOTA2, NOTA3, NOME) VALUES (?, ?, ?, ?)");
            ps.setString(1, disciplina.getNota1());
            ps.setString(2, disciplina.getNota2());
            ps.setString(3, disciplina.getNota3());
            ps.setString(4, disciplina.getNome());

            ps.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        } finally {
            ConectionFactory.close(null, ps, conn);
        }
    }

}
