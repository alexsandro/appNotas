package br.com.estacio.notas.model;

import java.io.Serializable;
import java.util.Objects;
   
  public class Usuario implements Serializable {
     
      private String login;
      private String senha;
      private String nome;
      private String ehProfessor;
   
      public String getNome() {
          return nome;
      }
   
      public void setNome(String nome) {
          this.nome = nome;
      }
   
      public String getLogin() {
          return login;
      }
   
      public void setLogin(String login) {
          this.login = login;
      }
   
      public String getSenha() {
          return senha;
      }
   
      public void setSenha(String senha) {
          this.senha = senha;
      }
   
      public String getEhProfessor() {
          return ehProfessor;
      }
   
      public void setEhProfessor(String ehProfessor) {
          this.ehProfessor = ehProfessor;
      }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.login, other.login)) {
            return false;
        }
        if (!Objects.equals(this.senha, other.senha)) {
            return false;
        }
        if (!Objects.equals(this.ehProfessor, other.ehProfessor)) {
            return false;
        }
        return true;
    }
      
  }
