package br.com.estacio.notas.servlet;

import br.com.estacio.notas.dao.UsuarioDAO;
import br.com.estacio.notas.model.Usuario;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "LoginServlet", urlPatterns = {"/login", "/logout"})
public class LoginServlet extends HttpServlet {

    private final String BT_AUTENTICAR = "bt_autenticar";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean isErro = false;
        String mensagem = null;
        String login = request.getParameter("login");
        String senha = request.getParameter("senha");
        if (login == null || login.isEmpty() || senha == null || senha.isEmpty()) {
            mensagem = "Login ou senha inválidos ou não informados!";
            System.out.println("Login ou senha inválidos ou não informados");
            isErro = true;
        }
        if (!isErro) {
            UsuarioDAO usuarioDAO = new UsuarioDAO();
            Usuario user = usuarioDAO.getObjeto(login);
            if (user != null) {
                if (user.getSenha().equalsIgnoreCase(senha)) {
                    request.getSession().setAttribute("usuarioLogado", user);
                    //TODO redirecionar para pagina do menu
                    System.out.println("Sucesso!");
                    request.getRequestDispatcher("/index.jsp").forward(request, response);
                    return;
                } else {
                    System.out.println("Login ou senha inválidos ou não informados");
                    mensagem = "Login ou senha inválidos ou não informados!";
                    isErro = true;
                }
            } else {
                System.out.println("Login ou senha inválidos ou não informados");
                mensagem = "Login ou senha inválidos ou não informados!";
                isErro = true;
            }
        }

        System.out.println("Chamou o invalidate");
        System.out.println(isErro);
        request.getSession().invalidate();
        request.setAttribute("mensagem", mensagem);
        request.setAttribute("contemErro", isErro);

        request.getRequestDispatcher("/login.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        processRequest(request, response);
        String uri = request.getRequestURI();
        System.out.println(uri);
        
        if (uri.toUpperCase().contains("LOGOUT")) {
            request.getSession().invalidate();
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
