/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.estacio.notas.servlet;

import br.com.estacio.notas.dao.AlunoDAO;
import br.com.estacio.notas.dao.AlunoDisciplinaDAO;
import br.com.estacio.notas.dao.DisciplinaDAO;
import br.com.estacio.notas.model.Aluno;
import br.com.estacio.notas.model.AlunoDisciplina;
import br.com.estacio.notas.model.Disciplina;
import br.com.estacio.notas.repositorio.Alunos;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "NotaServlet", urlPatterns = {"/NotaServlet", "/addNota", "/calcularMedia", "/addNotas"})
public class NotaServlet extends HttpServlet {
    
    private AlunoDAO alunoDAO = new AlunoDAO();
    private DisciplinaDAO disciplinaDAO = new DisciplinaDAO();
    private AlunoDisciplinaDAO alunoDisciplinaDAO = new AlunoDisciplinaDAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet NotaServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet NotaServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getRequestURI().contains("addNotas")) {
            request.setAttribute("matricula", request.getParameter("matricula"));
            request.setAttribute("disciplina", request.getParameter("disciplina"));

            //Aluno aluno = Alunos.getAlunoPorMatricula(request.getParameter("matricula"));
            Aluno aluno = alunoDAO.getObjeto(request.getParameter("matricula"));
            Disciplina d = null;
            
            List<AlunoDisciplina> listaAD =  alunoDisciplinaDAO.listarPorMatricula(aluno.getMatricula());
            List<Disciplina> listaDeDisciplinas = new ArrayList<>();
            for (AlunoDisciplina ad : listaAD) {
                listaDeDisciplinas.add(disciplinaDAO.getObjeto(ad.getNomeDisciplina()));
            }
            
            for (Disciplina dis : listaDeDisciplinas) {
                if (dis.getNome().equals(request.getParameter("disciplina"))) {
                    d = dis;
                }
            }
            request.setAttribute("nota1", d.getNota1());
            request.setAttribute("nota2", d.getNota2());
            request.setAttribute("nota3", d.getNota3());
            request.getRequestDispatcher("/addNotas.jsp").forward(request, response);
        } else {
            String matricula = request.getParameter("matricula");
            request.setAttribute("matricula", matricula);

           // Aluno aluno = Alunos.getAlunoPorMatricula(matricula);
            Aluno aluno = alunoDAO.getObjeto(matricula);
            
            for (Disciplina disciplinasMatriculada : aluno.getDisciplinasMatriculadas()) {
                System.err.println(disciplinasMatriculada.getNome());
            }

            request.setAttribute("disciplinas", aluno.getDisciplinasMatriculadas());
            request.getRequestDispatcher("/notasForm.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String matricula = request.getParameter("matricula");
        Float nota1 = Float.parseFloat(request.getParameter("nota1"));
        Float nota2 = Float.parseFloat(request.getParameter("nota2"));
        Float nota3 = Float.parseFloat(request.getParameter("nota3"));

        //Aluno atual = Alunos.getAlunoPorMatricula(matricula);
        Aluno atual = alunoDAO.getObjeto(matricula);
        Disciplina d = null;
        List<AlunoDisciplina> listaAD =  alunoDisciplinaDAO.listarPorMatricula(matricula);
            for (AlunoDisciplina ad : listaAD) {
                if (ad.getNomeDisciplina().equals(request.getParameter("disciplina"))) {
                     d = disciplinaDAO.getObjeto(ad.getNomeDisciplina());
                    break;
                }
            }
        
        
        //Disciplina d = Alunos.getDisciplinaMatriculada(atual, request.getParameter("disciplina"));
        //Disciplina d = disciplinaDAO.getObjeto(matricula);
        
        d.setNota1(nota1.toString());
        d.setNota2(nota2.toString());
        d.setNota3(nota3.toString());

        Alunos.updateDisciplinaAluno(atual, d);

        Float media = new Float("0.0");

        int quantDisc = 0;
        for (Disciplina dis : atual.getDisciplinasMatriculadas()) {
            Float n1 = (dis.getNota1() == null ? new Float("0.0") : Float.parseFloat(dis.getNota1()));
            Float n2 = (dis.getNota2() == null ? new Float("0.0") : Float.parseFloat(dis.getNota2()));
            Float n3 = (dis.getNota3() == null ? new Float("0.0") : Float.parseFloat(dis.getNota3()));

            media += (n1 + n2 + n3) / 3;
            quantDisc++;
        }
        atual.setMedia(media / quantDisc);
        //Alunos.update(atual);
        alunoDAO.atualizar(atual);
        response.sendRedirect("/AppNotas/alunos");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
}
