package br.com.estacio.notas.conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
   
  public final class ConectionFactory {
   
      private static final String DRIVER_DATABASE = "com.mysql.jdbc.Driver";
      private static final String USER_DATABASE = "root";
      private static final String PASS_DATAABASE = "root";
      private static final String URL_DATABASE = "jdbc:mysql://localhost:3306/appNotas";
   
      public static Connection conect() {
          try {
              Class.forName(DRIVER_DATABASE);
              return DriverManager.getConnection(URL_DATABASE, USER_DATABASE, PASS_DATAABASE);
          } catch (Exception exception) {
              exception.printStackTrace();
              return null;
          }
      }
   
      public static void close(ResultSet rs, Statement st, Connection conn) {
          if (rs != null) {
              try {
                  rs.close();
              } catch (SQLException exception) {
                  exception.printStackTrace();
              }
          }
          if (st != null) {
              try {
                  st.close();
              } catch (SQLException exception) {
                  exception.printStackTrace();
              }
          }
          if (conn != null) {
              try {
                  conn.close();
              } catch (SQLException exception) {
                  exception.printStackTrace();
              }
          }
      }
   
      public static void close(Statement statement, Connection connection) {
          close(null, statement, connection);
      }
   
      public static void close(Connection connection) {
          close(null, null, connection);
      }
      
  }
